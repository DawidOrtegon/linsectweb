<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.util.*" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Linsect Find By Name</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Knight - v4.2.0
    * Template URL: https://bootstrapmade.com/knight-free-bootstrap-theme/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

        <div class="logo">
            <a href="index.html"><img src="assets/img/LinsectB.pdf" alt="" class="img-fluid"></a>
        </div>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto active" href="index.html">Home</a></li>
                <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
                <li><a class="nav-link scrollto" href="#services">Services</a></li>
                <li><a class="nav-link scrollto " href="insectController">Check All Beetles</a></li>

                <li class="dropdown"><a href="#"><span>Menu</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="FindByName.jsp">Find by Name</a></li>
                        <li><a href="FindByDescription.jsp">Find by Description</a></li>
                    </ul>
                </li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header>
<!-- End Header -->

<!-- ======= Form ======= -->

<section id="contactA" class="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <form action="${pageContext.request.contextPath}/insectController" method="post" role="form">
                    <div class="container">
                        <input type="hidden" name="command" value="GET_BY_LOCATION">
                        <div class="form-group">
                            <label for="insectLocation">Insect Location</label>
                            <input type="text" class="form-control" name="location"/>
                        </div>
                        <div class="my-lg-3">
                            <div class="text-center">
                                <button type="submit" class="btn btn-info" style="background-color: #7CFC00FF; border: #6610f2">Search</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-------  End Form ------>

<!-- ======= Services Section ======= -->
<section id="services" class="services">
    <div class="container">

        <div class="section-title" data-aos="fade-up">
            <h2>What you can DO</h2>
            <p>Find, update and add beetles. Help us and help others to make Linsect the best not genetic database. </p>
        </div>

        <div class="row">
            <div class="col-lg-6 order-2 order-lg-1">
                <div class="icon-box mt-5 mt-lg-0" data-aos="fade-up">
                    <i class="bx bx-receipt"></i>
                    <h4>Find</h4>
                    <p>Check the current Database and check the beetles of your interest. </p>
                </div>
                <div class="icon-box mt-5" data-aos="fade-up" data-aos-delay="100">
                    <i class="bx bx-cube-alt"></i>
                    <h4>Add</h4>
                    <p>Complete the database with new beetles. Just One rule, not genetic aspects. Remember, this is for non biologists, it is for people passionate by nature.</p>
                </div>
                <div class="icon-box mt-5" data-aos="fade-up" data-aos-delay="200">
                    <i class="bx bx-images"></i>
                    <h4>Update</h4>
                    <p>If you have maybe got a better picture, ot just you have more information, update and in summary help. </p>
                </div>
                <div class="icon-box mt-5" data-aos="fade-up" data-aos-delay="300">
                    <i class="bx bx-shield"></i>
                    <h4>Keep your privacy</h4>
                    <p>Use all this functions without leaving information about you.</p>
                </div>
            </div>
            <div class="image col-lg-6 order-1 order-lg-2" style='background-image: url("assets/img/Services.jpg");' data-aos="fade-left" data-aos-delay="100"></div>
        </div>

    </div>
</section>
<!-- End Services Section -->

<section id="contact" class="contact section-bg">
    <div class="container">

        <div class="section-title">
            <h2>Contact</h2>
            <p>Let us know what you thing about Linsect. </p>
        </div>

        <div class="row">

            <div class="col-lg-12 text-center">
                <div class="info d-flex flex-column justify-content-center" data-aos="fade-center">
                    <div class="address">
                        <h4>Location:</h4>
                        <p>Macedonska 35/19<br>Wroclaw, 51-113</p>
                    </div>

                    <div class="email">
                        <h4>Email:</h4>
                        <p>251596@student.pwr.edu.pl</p>
                    </div>

                    <div class="phone">
                        <h4>Call:</h4>
                        <p>+48 889405784</p>
                    </div>

                </div>

            </div>
        </div>

    </div>
</section>
<!-- End Contact Section -->

<!-- ======= Footer ======= -->
<footer id="footer">

    <div class="footer-top">

        <div class="container">

            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <a href="#header" class="scrollto footer-logo"><img src="assets/img/Linsect.pdf" alt=""></a>
                    <p>Web Application to mainly manage and view not genetic information about beetles</p>
                </div>
            </div>


            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
            </div>

        </div>
    </div>

    <div class="container footer-bottom clearfix">
        <div class="copyright">
            &copy; Copyright <strong><span>Linsect</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/knight-free-bootstrap-theme/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="assets/vendor/aos/aos.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="assets/vendor/php-email-form/validate.js"></script>
<script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

<!-- Template Main JS File -->
<script src="assets/js/main.js"></script>

</body>

</html>