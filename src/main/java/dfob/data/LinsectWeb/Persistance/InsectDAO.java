package dfob.data.LinsectWeb.Persistance;

import dfob.data.LinsectWeb.Entities.Insect;

import java.io.IOException;
import java.util.List;

public interface InsectDAO
{
    // Select all the list of insects in the database.
    List<Insect> getAllInsects() throws IOException;

    // Select an insect by his ID.
    List<Insect> selectInsectByName(String insectName) throws IOException;

    // Find insect by his description.
    List<Insect> selectInsectByDescription(String insectDescription) throws IOException;

    //Find insect by his location
    List<Insect> selectInsectByLocation(String insectLocation) throws IOException;

}
