package dfob.data.LinsectWeb.Persistance;

import dfob.data.LinsectWeb.Entities.Insect;
import dfob.data.LinsectWeb.JDBCUtil;
import org.apache.commons.io.FileUtils;

import javax.sql.DataSource;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class InsectCRUD implements InsectDAO
{
    // Queries to the insect database.
    private static final String SELECT_ALL_INSECTS = "SELECT * FROM insectDatabase.insect;";
    private static final String SELECT_INSECT_BY_NAME = "SELECT * FROM insectDatabase.insect where name REGEXP ?";
    private static final String SELECT_INSECT_BY_DESCRIPTION = "SELECT * FROM insectDatabase.insect where description REGEXP ?";
    private static final String SELECT_INSECT_BY_LOCATION = "SELECT * FROM insectDatabase.insect where description REGEXP ?";

    private final DataSource dataSource;
    public InsectCRUD(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    // Override of the methods described in the DAO.
    @Override
    /**
     * Method to show all the insects inside the database.
     */
    public List<Insect> getAllInsects() throws IOException {
        List<Insect> insectList = new ArrayList<>();
        // 1. Get the connection to the database.

        try(Connection connection = JDBCUtil.getConnection())
        {
            // 2. Create the statement using the connection already created.
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_INSECTS);
            System.out.println(preparedStatement);

            // 3. Execute the query.
            ResultSet resultSet = preparedStatement.executeQuery();

            // 4. Process and organize the information obtained.
            while(resultSet.next())
            {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");

                // For only the picture.
                Blob picture = resultSet.getBlob("picture");
                InputStream binaryStream = picture.getBinaryStream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = binaryStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = output.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                binaryStream.close();
                output.close();

                String family =  resultSet.getString("family");
                String link = resultSet.getString("link");

                Insect P1 = new Insect(id, name, description, base64Image, family, link);
                P1.setBase64Image(base64Image);

                // 5. Adding the new attributes to the list Created in 1.
                insectList.add(P1);
            }

        }
        catch (SQLException  e) {
            e.printStackTrace();
        }

        return insectList;
    }

    @Override
    /**
     * Method to show an insect selected by his Id.
     */
    public List<Insect> selectInsectByName(String insectName) throws IOException {
        // Insect to be returned with the data coming from the query.
       List<Insect> insectList = new ArrayList<>();

        // 1. Establishing the connection.
        try(Connection connection =  JDBCUtil.getConnection())
        {
            // 2. Create the statement.
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_INSECT_BY_NAME);
            preparedStatement.setString(1,insectName);
            System.out.println(preparedStatement);

            // 3. Executing the query.
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println(resultSet);

            // 4. Process and organize the information obtained.
            while(resultSet.next())
            {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");

                // For only the picture.
                Blob picture = resultSet.getBlob("picture");
                InputStream binaryStream = picture.getBinaryStream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = binaryStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = output.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                binaryStream.close();
                output.close();

                String family =  resultSet.getString("family");
                String link = resultSet.getString("link");

                Insect P1 = new Insect(id, name, description, base64Image, family, link);
                P1.setBase64Image(base64Image);

                // 5. Adding the new attributes to the list Created in 1.
                insectList.add(P1);
            }

        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return insectList;
    }


    @Override
    /**
     * Get the beetle by his description.
     */
    public List<Insect> selectInsectByDescription(String insectDescription) throws IOException {
        // Insect to be returned with the data coming from the query.
        List<Insect> insectList = new ArrayList<>();

        // 1. Establishing the connection.
        try(Connection connection =  JDBCUtil.getConnection())
        {
            // 2. Create the statement.
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_INSECT_BY_DESCRIPTION);
            preparedStatement.setString(1,insectDescription);
            System.out.println(preparedStatement);

            // 3. Executing the query.
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println(resultSet);

            // 4. Process and organize the information obtained.
            while(resultSet.next())
            {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");

                // For only the picture.
                Blob picture = resultSet.getBlob("picture");
                InputStream binaryStream = picture.getBinaryStream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = binaryStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = output.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                binaryStream.close();
                output.close();

                String family =  resultSet.getString("family");
                String link = resultSet.getString("link");

                Insect P1 = new Insect(id, name, description, base64Image, family, link);
                P1.setBase64Image(base64Image);

                // 5. Adding the new attributes to the list Created in 1.
                insectList.add(P1);
            }

        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return insectList;
    }


    @Override
    /**
     * Get the insect by his location.
     */
    public List<Insect> selectInsectByLocation(String insectLocation) throws IOException {
        // Insect to be returned with the data coming from the query.
        List<Insect> insectList = new ArrayList<>();

        // 1. Establishing the connection.
        try(Connection connection =  JDBCUtil.getConnection())
        {
            // 2. Create the statement.
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_INSECT_BY_LOCATION);
            preparedStatement.setString(1,insectLocation);
            System.out.println(preparedStatement);

            // 3. Executing the query.
            ResultSet resultSet = preparedStatement.executeQuery();
            System.out.println(resultSet);

            // 4. Process and organize the information obtained.
            while(resultSet.next())
            {
                long id = resultSet.getLong("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");

                // For only the picture.
                Blob picture = resultSet.getBlob("picture");
                InputStream binaryStream = picture.getBinaryStream();
                ByteArrayOutputStream output = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = binaryStream.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }

                byte[] imageBytes = output.toByteArray();
                String base64Image = Base64.getEncoder().encodeToString(imageBytes);
                binaryStream.close();
                output.close();

                String family =  resultSet.getString("family");
                String link = resultSet.getString("link");

                Insect P1 = new Insect(id, name, description, base64Image, family, link);
//                P1.setBase64Image(base64Image);

                // 5. Adding the new attributes to the list Created in 1.
                insectList.add(P1);
            }

        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return insectList;
    }
}
