package dfob.data.LinsectWeb.Controllers;

import dfob.data.LinsectWeb.Entities.Insect;
import dfob.data.LinsectWeb.JDBCUtil;
import dfob.data.LinsectWeb.Persistance.InsectCRUD;
import dfob.data.LinsectWeb.Persistance.InsectDAO;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/insectController")
public class InsectController extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private InsectDAO insectDAO;
    private DataSource dataSource;

    public InsectController() {
        Context initContext = null;
        try {
            initContext = new InitialContext();
            Context envCtx = (Context) initContext.lookup("java:comp/env");
            // Look up our data source
            dataSource = (DataSource)
                    envCtx.lookup("jdbc/insectDatabase");

        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public void init() throws ServletException
    {
        super.init();
        try
        {
            insectDAO = new InsectCRUD(dataSource);
        }
        catch (Exception e) {
            throw new ServletException(e);
        }
    }

    // Main POST METHOD.
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doGet(request, response);
    }

    // Main GET METHOD.
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            String command = request.getParameter("command");

            if (command == null) {
                command = "LIST";
            }

            switch (command)
            {
                case "LIST":
                    listAllInsects(request, response);
                    break;

                case "GET_BY_NAME":
                    getInsectByName(request, response);
                    break;

                case "GET_BY_DESCRIPTION":
                    getInsectByDescription(request, response);
                    break;

                case "GET_BY_LOCATION":
                    getInsectByLocation(request,response);
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // Get the insects by the NAME.
    private void getInsectByName(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String insectName = request.getParameter("name");
        List<Insect> newInsect = insectDAO.selectInsectByName(insectName);

        request.setAttribute("insectList", newInsect);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/InsectsTable.jsp");
        dispatcher.forward(request,response);

    }

    // Get ALL the insects.
    private void listAllInsects(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        // Get the information from the CRUD.
        List<Insect> insectList = insectDAO.getAllInsects();

        // Sending the information to the view layer in the JSP.
        request.setAttribute("insectList", insectList);
        System.out.println(insectList.size());
        RequestDispatcher dispatcher = request.getRequestDispatcher("/InsectsTable.jsp");
        dispatcher.forward(request,response);
    }

    // Get the insects by the DESCRIPTION
    private void getInsectByDescription(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String insectDescription = request.getParameter("description");
        List<Insect> newInsect = insectDAO.selectInsectByDescription(insectDescription);

        request.setAttribute("insectList", newInsect);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/InsectsTable.jsp");
        dispatcher.forward(request,response);

    }

    // Get the insects by LOCATION.
    private void getInsectByLocation(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String insectLocation = request.getParameter("location");
        List<Insect> newInsect = insectDAO.selectInsectByLocation(insectLocation);

        request.setAttribute("insectList", newInsect);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/InsectsTable.jsp");
        dispatcher.forward(request,response);
    }



}
