package dfob.data.LinsectWeb.Entities;
import java.awt.*;
import java.util.Base64;

public class Insect
{
    // Attributes or columns for the table.
    private Long id;
    private String Name;
    private String Description;
    private byte[] Picture;
    private String base64Image;
    private String Family;
    private String Link;

    // Constructor without the Id.
    public Insect(String name, String description, byte[] picture, String family, String link) {
        Name = name;
        Description = description;
        Picture = picture;
        Family = family;
        Link = link;
    }

    // Empty Constructor.
    public Insect() {
    }

    // Constructor with id.
    public Insect(Long id, String name, String description, byte[] picture, String family, String link) {
        this.id = id;
        Name = name;
        Description = description;
        Picture = picture;
        Family = family;
        Link = link;
    }

    // Constructor with the String for the base 64 image.
    public Insect(Long id, String name, String description, String base64Image, String family, String link) {
        this.id = id;
        Name = name;
        Description = description;
        this.base64Image = base64Image;
        Family = family;
        Link = link;
    }

    // Setter and getter.
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public byte[] getPicture() {
        return Picture;
    }

    public void setPicture(byte[] picture) {
        Picture = picture;
    }

    public String getFamily() {
        return Family;
    }

    public void setFamily(String family) {
        Family = family;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }

    public String getBase64Image() {
//        base64Image = Base64.getEncoder().encodeToString(this.Picture);
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }
}
